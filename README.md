# Vremenska prognoza

Prvi projekat iz predmeta "Interakcija čovek računar"

### Tehnologije koje sam koristio za izradu projekta
* **HTML**
* **CSS**
* **Bootstrap 4**
* **Bootstrap Material Design**
* **Javascript**
* **JQuery**

### Pokratanje projekta

Ne postoje preduslovi za pokretanje projekta.
U narednim koracima je predstavljeno kako skinuti i pokrenuti projekat.

Potrebno je skinuti projekat na računar ručno, ili ga klonirati sa repozitorijuma putem konzole tako što ćete uneti sledeću komandu

```
git clone https://gitlab.com/Veljkic98/hci.git
```

Zatim je, u folderu u kom se nalazi projekat, potrebno pokrenuti server, kako bi se mogao čitati tekstualni fajl sa gradovima.

Primer za Python 2.x

```
python -m SimpleHTTPServer
```

Primer za Python 3.x

```
python -m http.server
```

zatim na vašem web browser-u unesite

```
http://localhost:8000/indeks.html
```

