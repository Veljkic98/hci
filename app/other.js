var cities = [];
var citiesJSON;
var city_data;
// Read txt file, server have to run in background
$.get('../text/city_list.txt', function(data) {
    citiesJSON = JSON.parse(data);
    make_list_of_cities(citiesJSON);
    
 }, 'text');

function make_list_of_cities(data) {
    for (var i = 0; i < data.length; i++){
        cities.push(data[i].name);
    }
    cities.sort();
    autocomplete(document.getElementById("search_term"), cities);
    load_weather("Novi Sad");
}


/* Code from w3school */
function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        var num = 0;
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                num++;
                if (num === 9) {
                    break;
                }
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
            /* Call method to find lat and long */
            load_weather(document.getElementById("search_term").value);
            }
        }
    });
    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
        if (elmnt != x[i] && elmnt != inp) {
            x[i].parentNode.removeChild(x[i]);
        }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

/*
    Load weather and fill up elements
*/
function load_weather(cityName)
{
    const key = 'c9ce6346bb12d7cc5af4de21ba6e018b'; //my key for OpenWeather api
    var lat;
    var lon;

    for (i = 0; i < citiesJSON.length; i++) {
        if (citiesJSON[i].name === cityName) {
            var city_item = citiesJSON[i];
            /* city and country to current weather */
            document.getElementById("location").innerHTML = city_item.name + ", " + city_item.country;
            lat = Number(city_item.coord.lat);
            lon = Number(city_item.coord.lon);
            break;
        }
    }

    /* https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&appid={YOUR API KEY} */
    fetch('https://api.openweathermap.org/data/2.5/onecall?' + 'lat=' + Number(lat) + '&lon=' + Number(lon) + '&units=metric&appid=' + key)
    .then(function(response) { return response.json() })
    .then(function(data) {
        city_data = data;
        set_current_weather(data);
        create_and_set_days_weahter(data.daily);
        createHourlyElements(data.hourly);
    })
}

/*
    site for weather icons: "https://erikflowers.github.io/weather-icons/"
    set current weather on top of the page
*/
function set_current_weather(weather) 
{
    /* temperature in celsius and  */
    document.getElementById("current-temperature").innerHTML = (Math.round(Number(weather.current.temp))).toString() + " <i id='icon-thermometer' class='wi wi-thermometer'>";
    set_weather_condition(weather, null);
    /* set weather description */
    var wcd = weather.current.weather[0].description;
    document.getElementById("weather_curr_desc").innerText = (wcd == undefined) ? "/" : wcd;
    var hum = weather.current.humidity;
    document.getElementById("humidity").innerText = (hum == undefined) ? "/" : hum +  " %";
    var wind =  weather.current.wind_speed;
    document.getElementById("wind").innerHTML = (wind == undefined) ? "/" : wind + " m/s";
    var vis = weather.current.visibility;
    document.getElementById("visibility").innerHTML = (vis == undefined) ? "/" : vis + " m";
    /* set temp for days. Forward list od days */
    
}

/*
    setting weather condition description icon
*/
function set_weather_condition(weather, dh_weather_icon)
{
    var iconName = "asd", iconClassName;

    if (dh_weather_icon === null) {
        iconName = weather.current.weather[0].icon;
    } else {
        iconName = dh_weather_icon;
    }

    var dayIcons = ["wi-day-sunny", "wi-day-cloudy", "wi-cloud", "wi-cloudy", "wi-hail", "wi-rain", "wi-thunderstorm", "wi-snow", "wi-fog"];
    var nighIcons = ["wi-night-clear", "wi-night-alt-cloudy", "wi-cloud", "wi-cloudy", "wi-hail", "wi-rain", "wi-thunderstorm", "wi-snow", "wi-fog"];

    var dayIconNames = ["01d", "02d", "03d", "04d", "09d", "10d", "11d", "13d", "50d"];
    var nightIconNames = ["01n", "02n", "03n", "04n", "09n", "10n", "11n", "13n", "50n"];

    for (i = 0; i < dayIcons.length; i++) {
        if (iconName.toUpperCase() === dayIconNames[i].toUpperCase()) iconClassName = "wi " + dayIcons[i];
        if (iconName.toUpperCase() === nightIconNames[i].toUpperCase()) iconClassName = "wi " + nighIcons[i];
    }

    if (dh_weather_icon === null) document.getElementById("icon-desc-current").className = iconClassName;
    return iconClassName;
}

/* 
    don't read from input without this 
*/
$('#search_term').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        alert('You pressed a "enter" key in textbox');  
    }
    //Stop the event from propogation to other handlers
    //If this line will be removed, then keypress event handler attached 
    //at document level will also be triggered
    event.stopPropagation();
});

/*
    Create 8 divs for days. Today and 7 coming days.
*/ 
function create_and_set_days_weahter(days)
{
    createDailyElements(days);
    // createHourlyElements();
    
}

/* 
    create daily table 
*/
function createDailyElements(days) 
{
    var itm = document.getElementById("proba1");
    var htmlObject;
    var dailyElem;
    var parser = new DOMParser();
    
    var day, min, max, iconClassName = "", date;

    var weekday = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var icons = [];

    /* list with 8 icons for 8 days */
    for (r = 0; r < 8; r++) {
        icons.push(set_weather_condition(null, days[r].weather[0].icon));
    }
    
    try {
        for (i = 0; i < 8; i++) {
            var elName = "#proba2-" + i.toString();
            $(elName).remove();
        }
    } catch {
    }

    for (i = 0; i < 8; i++) {
        date = new Date(Number(days[i].dt) * 1000);
        day = weekday[date.getDay()] + ", " + (date.getDate());

        min = Math.round(Number(days[i].temp.min)).toString() + "&#8451;";
        max = Math.round(Number(days[i].temp.max)).toString() + "&#8451;";

        iconClassName = icons[i];
        var divId = "proba2-" + i.toString();

        dailyElem = "";
        dailyElem = "<div id='" + divId + "' class='col daily' style='margin-right: 0.4rem; margin-left: 0.4rem;'>";
        dailyElem +=     "<div class='row'>";
        dailyElem +=         "<span id='day' style='font-size: 1.8rem; padding-left: 1rem;'>"+ day +"</span>";
        dailyElem +=     "</div>";
        dailyElem +=     "<div class='row' style='padding-bottom: 0.5rem;'>";
        dailyElem +=         "<div class='col align-self-center description-daily'>";
        dailyElem +=             "<i id='icon-desc-daily' class='" + iconClassName + "'></i>"	;	
        dailyElem +=         "</div>";
        dailyElem +=     "</div>";
        dailyElem +=     "<div class='row'>";
        dailyElem +=         "<div class='col temp'>";
        dailyElem +=             "<span style='font-size: 1.6rem;'>Max.</span>";
        dailyElem +=         "</div>";
        dailyElem +=         "<div class='col temp'>";
        dailyElem +=             "<span style='font-size: 1.6rem;'>Min.</span>";
        dailyElem +=         "</div>";
        dailyElem +=     "</div>";
        dailyElem +=     "<div class='row'>";
        dailyElem +=         "<div class='col temp'>";
        dailyElem +=             "<span id='max-temp' style='font-size: 1.6rem;'>" + max + "</span>";
        dailyElem +=         "</div>";
        dailyElem +=         "<div class='col temp'>";
        dailyElem +=             "<span id='min-temp' style='font-size: 1.6rem;'>" + min + "</span>";
        dailyElem +=         "</div>";
        dailyElem +=     "</div>";
        dailyElem += "</div>";

        htmlObject = parser.parseFromString(dailyElem, 'text/html').getElementById(divId);
        itm.appendChild(htmlObject);
    }
}

function createHourlyElements(hourly)
{
    var itm = document.getElementById("ciId");
    var htmlObject, htmlObject2;
    var carouselElem, hourlyElem;
    var parser = new DOMParser();
    var temp, date, idNum = 0, time, press, hum;
    var icons = [];
    var divId, divId1, divId2;

    for (r = 0; r < 48; r++) {
        icons.push(set_weather_condition(null, hourly[r].weather[0].icon));
    }

    /* first clear old */
    try {
        for (i = 0; i < 6; i++) {
            var elName = "#nesto-" + i.toString();
            $(elName).remove();
        }
    } catch {
        console.log("not clear ?");
    }

    for (i = 0; i < 6; i++) {
        if (i == 0) active = "active"

        var active = (i == 0) ? "active" : "";

        divId = "nesto-" + i.toString();
        divId1 = "cfId-" + i.toString();

        carouselElem = "";
        carouselElem += "<div id='nesto-" + i + "' class='carousel-item " + active + "'><div id='cfId-" + i + "' class='container-fluid' style='width: 100%; padding-top: 1.4rem; padding-bottom: 6rem; padding-left: 5rem; padding-right: 5rem;'><div id='rowId-" + i + "' class='row'></div></div></div>"

        htmlObject = parser.parseFromString(carouselElem, 'text/html').getElementById(divId);

        /* add 8 hour items to container */
        for (j = 0; j < 8; j++) {
            divId2 = "hour-" + idNum;
            date = new Date(Number(hourly[idNum].dt) * 1000);
            temp = Math.round(Number(hourly[idNum].temp));
            press = hourly[idNum].pressure;
            hum = hourly[idNum].humidity;

            if (i == 0 && j == 0) time = "NOW";
            else {
                time = date.getHours().toString();
                if (time.length == 1) time = "0" + time;
                time += ":00"
            }

            hourlyElem = "<div id='hour-" + idNum + "' class='col daily' style='margin-right: 0.5rem; margin-left: 0.5rem;'>" +
                "<div class='row'>"+
                    "<span id='timeId' style='font-size: 1.8rem; padding-left: 1rem;'>" + time + "</span>"+
                "</div>"+
                "<div class='row' style='padding-bottom: 0.5rem;'>"+
                    "<div class='col align-self-center description-daily'>"+
                        "<i id='icon-desc-daily' class='" + icons[idNum] + "'></i>"	+		
                    "</div>"+
                "</div>"+
                "<div class='row'>"+
                    "<div class='col temp' style='text-align: center;'>"+
                    "<i id='icon-desc-baro' class='wi wi-thermometer' style='font-size: 1.6rem;'></i>"+
                        "<span id='maxTempId' style='font-size: 1.6rem;'>&nbsp;"+temp+"</span>"+ //&nbsp;&#8451;
                    "</div>"+
                "</div>"+
                "<div class='row'>"+
                    "<div class='col temp' style='text-align: center;'>"+
                        "<i id='icon-desc-baro' class='wi wi-barometer' style='font-size: 1.6rem;'></i>"+
                        "<span style='font-size: 1.6rem;'>&nbsp;" + press + "</span>"+
                    "</div>"+
                "</div>"+
                "<div class='row'>"+
                    "<div class='col temp' style='text-align: center;'>"+
                        "<i id='icon-desc-baro' class='wi wi-humidity' style='font-size: 1.6rem;'></i>"+
                        "<span style='font-size: 1.6rem;'>&nbsp;" + hum + "</span>"+
                    "</div>"+
                "</div>"+
            "</div>"
            idNum++;

            htmlObject2 = parser.parseFromString(hourlyElem, 'text/html').getElementById(divId2);
            htmlObject.querySelector("#rowId-" + i).appendChild(htmlObject2);
        }
        itm.appendChild(htmlObject);
    }

    createChart("Temperature");
}

var val1 = 0, val2 = 12;
/* take value from sliders */
$(document).ready(function() {
    const $valueSpan2 = $('.valueSpan2');
    const $valueSpan3 = $('.valueSpan3');
    const $value11 = $('#customRange11');
    const $value22 = $('#customRange22');
    $valueSpan2.html($value11.val());
    $valueSpan3.html($value22.val());
    
    $value11.on('input change', () => {
        $valueSpan2.html($value11.val());
        val1 = Number($value11.val()) - 1;
    });

    $value22.on('input change', () => {
        $valueSpan3.html($value22.val());
        val2 = Number($value22.val()) - 1;
    });
});

/*
    Create chart method
*/
var chart_num = -1;
function createChart(label)
{
    chart_num++;

    var max, min;
    if (val1 > val2) {
        max = val1; min = val2;
    } else {
        max = val2; min = val1;
    }

    if (min == 25 && max == 25) {
        min = 0; max = 12;
    }

    var labels = [], data = [];

    var date;
    for (i = min; i < max; i++) {
        date = new Date(Number(city_data.hourly[i].dt) * 1000);
        labels.push(date.getHours().toString());
        
        if (label == "Temperature") data.push(Number(city_data.hourly[i].temp));
        else if (label == "Presure") data.push(Number(city_data.hourly[i].pressure));
        else data.push(Number(city_data.hourly[i].humidity));
    }

    /* 
        Chart implementation 
        Delete old and create new
    */
    var parser = new DOMParser();
    idName = "lineChart";
    document.getElementById("chart").remove();
    var itm = document.getElementById("grafik2");
    var graphic = "<div class='container cts' id='chart'></div>";
    htmlObject = parser.parseFromString(graphic, 'text/html').getElementById("chart");
    htmlObject.innerHTML = "<canvas id='lineChart'></canvas>";
    itm.appendChild(htmlObject);

    var ctxL = document.getElementById(idName).getContext('2d');
    var myLineChart = new Chart(ctxL, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: label,
                data: data,
                backgroundColor: [
                    'rgba(105, 0, 132, .2)',
                ],
                borderColor: [
                    'rgba(200, 99, 132, .7)',
                ],
                borderWidth: 2
            },
            ]
        },
        options: {
            responsive: true
        }
    });
}

/*
    Function to hide element by id
*/
function hideElem(elemId) {
    var x = document.getElementById(elemId);
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

/*
    Wait for n seconds
    ms = 1000 => 1sec
*/
function wait(ms){
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms) {
      end = new Date().getTime();
   }
}
